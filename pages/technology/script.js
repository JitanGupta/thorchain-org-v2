import {fetchTechnologyViewData} from '../../utils/commonFunctions';
export default {
    data() {
        return {
            minimumBond: null,
            bondInUsd: null,
            asgardActiveVaultLink: "dyanmic",
            avatars: [
                {
                    img: require('~/assets/images/external-avatars/thorchain.png'),
                    name: 'ThorChain',
                    tag: 'Introducing Community Pros!',
                    socialLinks: {
                        github: '',
                        gitlab: '',
                        telegram: '',
                        twitter: ''
                    },
                    active: false,
                    thorchain: true
                },
                {
                    img: require('~/assets/images/external-avatars/Delphi.jpg'),
                    name: 'Delphi_Digital',
                    tag: '@Delphi_Digital',
                    socialLinks: {
                        github: '',
                        gitlab: '',
                        telegram: this.$links("telegram_delphi_digital"),
                        twitter: this.$links("twitter_delphi_digital")
                    },
                    active: false
                },
                {
                    img: require('~/assets/images/external-avatars/perklin.jpg'),
                    name: 'mperklin',
                    tag: '@mperklin',
                    socialLinks: {
                        github: '',
                        gitlab: '',
                        telegram: this.$links("telegram_mperklin"),
                        twitter: this.$links("twitter_mperklin")
                    },
                    active: false
                },
                {
                    img: require("~/assets/images/avatars/sample-avatar.svg"),
                    name: 'tannedoaksprout',
                    tag: '@tannedoaksprout',
                    socialLinks: {
                        gitlab: '',
                        github: '',
                        telegram: '',
                        twitter: this.$links("twitter_tannedoaksprout")
                    },
                    active: false
                },
                {
                    img: require('~/assets/images/external-avatars/CanGure.jpg'),
                    name: 'CanGure91596181',
                    tag: '@CanGure91596181',
                    socialLinks: {
                        gitlab: '',
                        github: '',
                        telegram: '',
                        twitter: this.$links("twitter_cangure91596181")
                    },
                    active: false
                },
                {
                    img: require('~/assets/images/external-avatars/kalra.jpg'),
                    name: 'Jatinkkalra',
                    tag: '@Jatinkkalra',
                    socialLinks: {
                        gitlab: '',
                        github: '',
                        telegram: this.$links("telegram_jatinkkalra"),
                        twitter: this.$links("twitter_jatinkkalra")
                    },
                    active: false
                },
                {
                    img: require('~/assets/images/external-avatars/twitter-rune-ranger.jpg'),
                    name: 'TheRuneRanger',
                    tag: '@TheRuneRanger',
                    socialLinks: {
                        gitlab: '',
                        github: '',
                        telegram: '',
                        twitter: this.$links("twitter_theruneranger")
                    },
                    active: false
                },
            ],
            butSirs: {
                firstButSir: {
                    context: this.$t('technicalPage.firstButSir'),
                    isRight: true,
                    height: "14.187rem",
                    imgStyle: {
                        maxWidth: '12.5rem',
                        width: '12.3125rem',
                        margin: '0 1.125rem 0 -3.75rem'
                    },
                    linkDisabled: [false, false, false, false],
                    imgSrc: require('~/assets/images/technical-page/looking-through-binoculars.svg'),
                    imgAlt: "thorchain node",
                    links: [this.$links('twitter_new_chain'), 
                    this.$links('docs_node_operations'), 
                    this.$links('viewblock'), 
                    this.$links('telegram_dev')]
                },
                secondButSir: {
                    context: this.$t('technicalPage.secondButSir'),
                    isRight: false,
                    height: "18.5rem",
                    imgStyle: {
                        width: '11.125rem',
                        maxWidth: '10.525rem'
                    },
                    linkDisabled: [false],
                    imgSrc: require('~/assets/images/technical-page/surprised-man.svg'),
                    imgAlt: "thorchain node",
                    externalClasses: ['vault']
                },
                thirdButSir: {
                    context: this.$t('technicalPage.thirdButSir'),
                    isRight: false,
                    height: "26.812rem",
                    imgStyle: {
                        maxWidth: '23.6rem',
                        width: '23.625rem'
                    },
                    linkDisabled: [false, false, false],
                    imgSrc: require('~/assets/images/technical-page/thief.svg'),
                    imgAlt: "thorchain node",
                    links: ["dynamic", 
                    this.$links('viewblock_validators'), "/rune"],
                },
                rewiringButSir: {
                    context: this.$t('technicalPage.rewiringButSir'),
                    isRight: false,
                    height: "16rem",
                    imgStyle: {
                        maxWidth: '25.9rem',
                        width: '25.8125rem',
                        'margin-right': "-1rem"
                    },
                    linkDisabled: [false, false, false],
                    imgSrc: require('~/assets/images/technical-page/Sirs.svg'),
                    imgAlt: "thorchain node",
                    links: [this.$links('twitter_integrating'), this.$links('medium_affiliate_fees'), this.$links('docs_midgard_api')]
                },
                wheretoaskButSir: {
                    context: this.$t('technicalPage.wheretoaskButSir'),
                    isRight: true,
                    height: "17.687rem",
                    imgStyle: {
                        maxWidth: '15.975rem',
                        width: '15.975rem',
                        marginLeft: '0'
                    },
                    linkDisabled: [false, false, false, false],
                    imgSrc: require('~/assets/images/technical-page/helping-wounded-man.svg'),
                    imgAlt: "thorchain node",
                    linkTemp: [0,0,0,1],
                    links: [this.$links('twitter'), 
                    this.$links('telegram_community'), 
                    this.$links('discord'), 
                    this.$links('medium_liquidity_pooling')],
                    linkMargin: "1.625rem"
                },
                smokeButSir: {
                    context: this.$t('technicalPage.smokeButSir'),
                    isRight: true,
                    height: "16rem",
                    imgStyle: {
                        maxWidth: '11.3rem',
                        width: '11.5rem',
                        marginLeft: '-1.875rem'
                    },
                    linkDisabled: [false, false, false, false, false],
                    imgSrc: require('~/assets/images/technical-page/friends-talking.svg'),
                    imgAlt: "thorchain node",
                    linkTemp: [0,1,0,0,0],
                    links: [this.$links('asgardex_web'), 
                    this.$links('medium_liquidity_pooling'), 
                    this.$links('twitter_arbitraging'), 
                    this.$links('docs_node_operations'), 
                    this.$links('twitter_integrating')
                ]
                }
            }
        };
    },
    head() {
        return {

            title: 'Technology - THORChain',
            meta: [
                    {property: "og:locale", content: 'description'},
                    {property: "og:locale", content: "en_US"},
                    {property: "og:type", content: "website"},
                    {property: "og:title", content: "Technology - THORChain"},
                    {property: "og:description", content: "THORChain is a liquidity protocol based on Tendermint & Cosmos-SDK and utilising Threshold Signature Schemes (TSS) to create a marketplace of liquidity for digital assets to be traded in a trustless, permissionless & non-custodial manner."},
                    {property: "og:url", content: process.env.baseUrl + "/technology"},
                    {property: "og:site_name", content: "THORChain"},
                    {property: "og:image", content: process.env.baseUrl + "/technology-meta.png"},
                    {property: "og:image:width", content: "876"},
                    {property: "og:image:heigh", content: "438"},
                    {name: "twitter:creator", content: "@thorchain_org"},
                    {name: "twitter:site", content: "@thorchain_org"},
                    {name: "twitter:title", content: "Technology - THORChain"},
                    {name: "twitter:description", content: "THORChain is a liquidity protocol based on Tendermint & Cosmos-SDK and utilising Threshold Signature Schemes (TSS) to create a marketplace of liquidity for digital assets to be traded in a trustless, permissionless & non-custodial manner."},
                    {name: "twitter:card", content: "summary_large_image"},
                    {name: "twitter:image", content: process.env.baseUrl + "/technology-meta.png"},
            ],
        }

    },
    methods: {
        handleScroll() {
            this.$store.commit({
                type: "scrollChange",
                amount: window.scrollY,
            });
        },
        showThisAvatar(i) {
            this.avatars.forEach((ava, index) => {
                if (index == i) {
                    if (ava.active) {
                        ava.active = false
                        return
                    }
                    ava.active = true
                }
                else
                    ava.active = false
            })
        },
        clickOutside() {
            this.avatars.forEach(ava => {
                ava.active = false;
            })
        }
    },
    mounted() {
        if(this.butSirs && this.butSirs.thirdButSir && this.butSirs.thirdButSir.links)
            this.butSirs.thirdButSir.links[0] = this.asgardActiveVaultLink

        window.addEventListener('mousedown', (e) => {
            // console.log(e.target)
            console.log(e.target.compareDocumentPosition(document.querySelector(".community-pros")))
            if (e.target.compareDocumentPosition(document.querySelector(".community-pros")) !== 10){
                this.clickOutside();
            }
        });
    },
    beforeMount() {

        this.handleScroll();
        window.addEventListener("scroll", this.handleScroll);
        this.butSirs.thirdButSir.links[0] = this.asgardActiveVaultLink
        // console.log("beforeMount");
    },
    beforeDestroy() {
        window.removeEventListener("scroll", this.handleScroll);
        // console.log("beforeDestroy");
    },
    async asyncData({
        store, $services, $links
    }) {
        var {
            minimumBond,
            bondInUsd,
            asgardActiveVaultLink,
            totalTx
        } = await fetchTechnologyViewData($services)

        return {
            minimumBond,
            bondInUsd,
            asgardActiveVaultLink,
            totalTx
        }
    },
};