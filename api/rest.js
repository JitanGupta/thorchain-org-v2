const express = require('express')
const app = express()
const fetchers = require('./fetchers');
const fetchersV2 = require('./fetchersV2');

const e = require('express');
const { reportError } = require('./errorHandling');
const utils = require('../utils');
const telegramBot = require('./telegramBot');
const cors = require('cors')

const corsOptions = {
  origin: process.env.MOBILE_BASE_URL
}
app.use(express.json());
app.use(cors(corsOptions))
function createExpressApp() {
  const SERVER_START_TIME = Date.now()
  telegramBot.log("Starting the server")
  
  var reqCount = 0
  var data = {
    'coingeckoMarkets':{ fetcher: fetchersV2.fetchCoingeckoMarkets, updateEvery: 5 /*seconds*/ },
    'coingeckoNetInfo':{ fetcher: fetchersV2.fetchCoingeckoNetInfo, updateEvery: 5 },
    'activeAsgardVaultLink':{ fetcher: fetchersV2.fetchActiveAsgardVaultLink, updateEvery: 100},
    'runePrice':{ fetcher: fetchersV2.fetchRunePrice, updateEvery: 5 },
    'minBond':{ fetcher: fetchersV2.fetchMinimumBond, updateEvery: 100 },
    'lastBlock':{ fetcher: fetchersV2.fetchLastBlock, updateEvery: 1 },
    'thorNetValues':{ fetcher: fetchers.fetchThorNetValues, updateEvery: 10},
    'thorNetValuesMCCN':{ fetcher: fetchersV2.fetchThorNetValues, updateEvery: 10},
    'congecko_ERC20_Markets': {fetcher: fetchers.fetchCoingecko_ERC20_Markets, updateEvery: 5}
  }

  /* Update all the values at server init */
  setTimeout(async () => {
    for (var key of Object.keys(data)) {
      (() => {
        var currentKey = key
        var record = data[key]
        record['lastUpdate'] = Date.now()

        record.fetcher().then((res)=>{
            record['value'] = res
            record['err'] = null
        })
        .catch(rej =>{
          record['value'] = null
          record['err'] = rej
        })
      })()
    }
  }, 0);

  setInterval(async () => {
    for (var key of Object.keys(data)) {
      var record = data[key]

      /* update the record if it's the time */
      if (Date.now() - record.lastUpdate >= record.updateEvery * 1000) {

        (() => {
          var currentKey = key
          var record = data[key]
          record['lastUpdate'] = Date.now()
  
          record.fetcher().then((res)=>{
            if(res)
              record['value'] = res
              record['err'] = null
          })
          .catch(rej =>{
            record['value'] = null
            record['err'] = rej
            console.error(currentKey +": failed")
            console.error(rej)
          })
        })()

      }
    }
  }, 500);

  app.get('/api/static_data/:key', async (req, res)=>{      
    try{
      // console.log(`Server: ${req.url} called`)
      var key = req.params.key
      if(key in data){
        
        if(!data[key].value){
          telegramBot.log(`No values for '${key}'. Server is running for 
          ${utils.timeConversion(Date.now() - SERVER_START_TIME)}`)
          // reportError()
          // console.log(`Server: ${req.url} end`)
          return res.status(404).json({msg: "external api not responding", key})
        }
        
        var value = data[key].value
        
        console.log({
          url: req.url,
          value,
          err: data[key].err
        })
        res.json(value)
        // console.log(`Server: ${req.url} end`)

      }
      else{
        res.status(404).json({msg: 'Static data Not found', key})
      }
    }
    catch(e){
      console.error(e)
    }
  })

  app.get('/api/coingecko', (req, res) => {
    var coingecko = {
      markets: data.coingeckoMarkets? data.coingeckoMarkets.value: {},
      netInfo: data.coingeckoNetInfo? data.coingeckoNetInfo.value: {},
      lastUpdate: data.coingeckoMarkets? data.coingeckoMarkets.lastUpdate: 0
    }
    res.json(coingecko)

  })
  app.get('/api/rc', (req,res)=>{
    reqCount++
    res.json(reqCount)
  })
  app.get('/api/*', (req, res)=>{
    res.status(404).send({msg: 'Not found', url: req.url})
  })
  return app
}

module.exports = createExpressApp()