/* Adjust font-size */
var innerHeight = window.innerHeight
var innerWidth = window.innerWidth
console.log("Screen Width, Height is: "+ innerWidth + ", "+innerHeight)
var perfectScreenSize = {width: 1600, height: 900}

if(innerWidth > perfectScreenSize.width && innerHeight > perfectScreenSize.height){
  var newFontSize = 16
  console.log("No need to scale")
}
else if(innerWidth > perfectScreenSize.width && innerHeight < perfectScreenSize.height){
  var fontByWdith = 16;
  var newFontSize = (innerHeight / perfectScreenSize.height) * 16;
  if(newFontSize < fontByWdith * 0.8){
    newFontSize = fontByWdith * 0.8
  }
  console.log("Scaling down...")
}
else if(innerWidth < perfectScreenSize.width && innerHeight > perfectScreenSize.height){
  var newFontSize = (innerWidth / perfectScreenSize.width) * 16;
  console.log("Scaling down...")

}
else if(innerWidth < perfectScreenSize.width && innerHeight < perfectScreenSize.height){
  var fontByWdith = (innerWidth / perfectScreenSize.width) * 16;
  var fontByHeight = (innerHeight / perfectScreenSize.height) * 16;
  var newFontSize = fontByWdith < fontByHeight ? fontByWdith : fontByHeight;
  if(newFontSize < fontByWdith * 0.8){
    newFontSize = fontByWdith * 0.8
  }
  console.log("Scaling down...")
}
document.documentElement.style.fontSize = newFontSize + "px"
var html = document.documentElement,
style = window.getComputedStyle(html),
htmlFontSize = style.getPropertyValue('font-size')
console.log("Html Font size is: ",htmlFontSize)
// var self = this
// window.addEventListener("orientationchange", function() {
//   if(window.orientation == 0 && window.screen.width < 650){
//     self.redirectToMobile()
//   }
// }, false);