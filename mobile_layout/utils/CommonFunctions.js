
var utils = require('../../utils') 
module.exports =  {
    async getCoingeckoTable($services, $links){
        var tableNumberStyle = `
          font-family: Roboto Mono;
          font-style: normal;
          font-weight: normal;
          font-size: 0.875rem ;
          line-height: 1.125rem ;
          text-align: right;
          letter-spacing: 0rem ;
          color: #D1D5DA;
        `
        var res = await $services.getCoingeckoData()
        if (!res) return [];
        var newData = res.markets;
  
        var ERC20Markets = await $services.getERC20Markets()
        for(var eMarket of ERC20Markets){
          // replacing the smallest volume with the erc20 market
          function removeSmallest(markets) {
            var smallestNumberKEY = 0;
            for (var i = 0; i < markets.length - 1; i++) {
              if (markets[i + 1]['volume'] < markets[i]['volume']) {
                  smallestNumberKEY = i + 1;
              }
            }
            markets.splice(smallestNumberKEY, 1);
            return markets;
          }
          newData = removeSmallest(newData)
        }
        newData.push(...ERC20Markets)
        
        for (var i in newData) {
          newData[i]["key"] = JSON.stringify(newData[i]);
          if(newData[i].pair.length > 8) newData[i].pair = newData[i].pair.substr(0,8) +"..."
          if (newData[i].market == "BepSwap") {
            newData[i]["is_thor"] = true;
            newData[i].market += "⚡";
          }
          else newData[i]["is_thor"] = false;
          if(newData[i].market.length > 8) newData[i].market = newData[i].market.substr(0,8) +"..."
          
          newData[i]["volume"] = utils.nFormatter(parseInt(newData[i]["volume"]));
          
          newData[i]["depth_up"] = utils.numberWithCommas(
            parseInt(newData[i]["depth_up"])
          );
          newData[i]["depth_down"] = utils.numberWithCommas(
            parseInt(newData[i]["depth_down"]) 
          );
          newData[i]['market_link_props'] = JSON.parse(JSON.stringify($links('coingecko_pair')))
          newData[i]['market_link_props'].link = newData[i]['tradeUrl']
        }
        var rows = [
          //Header
          {cols: [
              {content: "Exchange", style: "width: 30%"},
              {content: "Pair", style:     "width: 30%"},
              {content: "Price", style:    "width: 20%; text-align: right;"},
              {content: "24HV", style:     "width: 20%; text-align: right"}
            ]
          }
        ]
      for(var record of newData){
          rows.push({
            cols: [
              {content:record.market, style:"width: 30%"},
              {content: record.pair, style: "width: 30%; color: #23DCC8"},
              {content: "$"+record.price, style: "width: 20%; text-align: right;"+tableNumberStyle},
              {content: record.volume, style: "width: 20%; text-align: right;"+tableNumberStyle}
            ],
            link: record.market_link_props
          })
        }
        rows.push(
              {cols: ['ShapeShift', 
              {content:'Buy ERC20 RUNE',
              style: "width:30%;",
              textStyle: "position: absolute;"},
              '', ''], 
              link: $links("shapeshift_mobile_app"), 
              style:"border-color: #000; background-color: #386FF9; color: #fff"}
        )
         rows.push(
           {
  
              cols: ['Data from CoinGecko', 
              {content:'Show more',
              style: "text-align: right; color: #23DCC8"},
              ], 
              link: $links("coingecko_show_more"),
              style: "border-color: #15242E"
           }
        )
        return rows
      }
}