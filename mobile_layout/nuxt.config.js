export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'mobile_layout',
    // htmlAttrs: {
    //   lang: 'en'
    // },
    title: 'THORChain.org - Decentralized Liquidity Network',
    
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, user-scalable=no' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: 'data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>⚡</text></svg>' }
    ],
    script: [
      { src: '/js/scaleBasedOnScreenSize.js' } /* file is located at './utils/js/scaleBasedOnScreenSize.js' */
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {src:'../plugins/linksV2.js'},
    {src:'../plugins/services.js'},
    { src: '~/plugins/youtube.js', ssr: false},
    { src: '~/plugins/vue-touch', ssr: false },
    { src: '~/plugins/vue-awesome-swiper', ssr: false },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/device'
  ],


  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',

    ['nuxt-i18n', {
      strategy: 'prefix_and_default',
      defaultLocale: 'en',
      lazy: 'true', 
      langDir: '../locales/', 
      locales:[  { code: 'en', iso: 'en-US', file: 'en.json', dir: 'ltr' },]}
    ],
    // https://go.nuxtjs.dev/bootstrap  
    'bootstrap-vue/nuxt',
    "@nuxtjs/svg",
    
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [
      "vue-loading-spinner"
    ]
  },

  css: [
    '@/assets/styles/_font.scss',
  ],
  router: {
    // Run the middleware/switchToDesktop.js on every page
    middleware: 'switchToDesktop'
  },
  env:{
    baseUrl: process.env.BASE_URL,
  },
  loading: false

}
